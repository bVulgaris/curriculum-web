$(window).load(function() {
    $("#loading").fadeOut();

    $("#logo").click(function(event){
        event.preventDefault()
        if ($(window).width() < 768){
            if($(".resposiveNavbar").is(":visible")){
                $(".resposiveNavbar").hide();
            }else{
                $(".resposiveNavbar").show();
            }
        }else{
            $("html, body").animate({ scrollTop: 0 }, "slow");
        }
    });

    $(".responsiveMenu").click(function(){
        $(".resposiveNavbar").hide();
    });
});

function eyes() {
    setTimeout(function(){
        $(".eyesclosed").hide();    
    }, 200);
}

setInterval(function(){
    $(".eyesclosed").show();
    eyes();
}, 3000);

$(function() {
    $(window).scroll(function(){
        /*
        if($(window).scrollTop() >= $("#inicio").height()){
            $('.menuSup').css('position','fixed').css("top","0").css('opacity','1');
            $('#logo').css('position','fixed').show("slow");
        }else {
            $('#logo').css('position','absolute').hide("slow");
            $('.menuSup').css('position','absolute').css('opacity','0');
        }
        */
        if($(".menu").offset().top >= $("#portafolio").offset().top && $(".menu").offset().top <= $("#contact").offset().top){
            $(".menuSup a").css("color", "black");
            $("#resumepdf img").attr("src","images/blackArrow.svg");
            $(".menu").addClass("fondoBlanco");
        }else{
            $(".menuSup a").css("color", "white");
            $("#resumepdf img").attr("src","images/arrow.svg");
            $(".menu").removeClass("fondoBlanco");
        }
    });
    
    // Select all links with hashes
    $('a[href*="#"]')
    // Remove links that don't actually link to anything
    .not('[href="#"]')
    .not('[href="#0"]')
    .click(function(event) {
    // On-page links
        if(location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname){
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if(target.length){
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000, function() {
                    // Callback after animation
                    // Must change focus!
                    var $target = $(target);
                    $target.focus();
                    if ($target.is(":focus")) { // Checking if the target was focused
                        return false;
                    }else {
                        $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
                        $target.focus(); // Set focus again
                    }
                });
            }
        }
    });

    $(".work").click(function(event){
        if ($(window).width() > 768){

            $(this).siblings().animate({height  :  "200px"}, 400);
            $(this).siblings().find(".workDescription").animate({height:"0px"}, 400);
            $(this).siblings().find(".arrow").hide();
            if($(this).css("height")=="550px"){
                $(this).animate({height  :  "200px"}, 400);
                $(this).find(".workDescription").animate({height:"0px"}, 400);
                $(this).find(".arrow").hide();
            }else{
                $(this).animate({height  :  "550px"}, 400);
                $(this).find(".workDescription").animate({height:"335px"}, 400);
                $(this).find(".arrow").show();
            }
        
        }else{
            $("body").css("overflow", "hidden");
            $("#responsiveContainer").css("top", $(".menu").offset().top)

            $(this).find(".workDescription").html();
            $("#responsiveContainer .respContent").append($(this).find(".workDescription").html());
            $("#responsiveContainer").show();
        }
        
    });

});


$( document ).ready(function() {

    $("#closeRespContainer").click(function(){
        $("body").css("overflow", "auto");
        $("#responsiveContainer").hide();
        $("#responsiveContainer .respContent").html("");
    });

    $( ".work" ).each(function( index ) {
    
        var portafolioContainerWith   = $(".portafolioContainer").width();
        var indexOfwork               = $(this).attr("data-index");
    
        var numOfWorks           = $('.work').length;
        var numWorksByFile       = Math.floor(portafolioContainerWith/200);      
    
        var PositionOfWorkOnFile;
        if(indexOfwork <= numWorksByFile){
            PositionOfWorkOnFile = indexOfwork;
        }else{
            PositionOfWorkOnFile = indexOfwork - (Math.floor((indexOfwork-1)/numWorksByFile)*numWorksByFile);
        }
    
        var left                 = (PositionOfWorkOnFile-1)*-200;
        var descripionMaxWith    = numWorksByFile*200;
        
        $(this).find(".workDescription").css("width", descripionMaxWith+"px").css("left",left+"px")

    });

});